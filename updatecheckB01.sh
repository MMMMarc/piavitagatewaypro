#!/bin/sh


# Update parameters. Feel free to change this!
FILENAME=B01I03F00
DEVICESW=M1m26




##########################################################
# WARNING: BE AWARE of changing everythin after this line!
# Upgrading script
# Check if there is a ID.txt file
cd /home/piavita/application

UPDATESERVER=$(sed -n 's|.*"updateserver":"\([^"]*\)".*|\1|p' HWsettings.json)
VERSION=$(sed -n 's|.*"BasestationSW":"\([^"]*\)".*|\1|p' SWsettings.json)
# check if software upgrade exists and if the old is aready finished
if [ $FILENAME = $VERSION ];then
  echo no update
  exit 0
fi
cd /home/piavita/application
#sudo node /home/piavita/application/LEDs_orange.js

#cd /home/piavita/application
#[ ! -d tmp ] && mkdir tmp && chown piavita:piavita tmp
#cd /home/piavita/application/tmp

#cd /home/piavita/application/tmp
#remove fertig file becaus the update is running to make sure it will run again if it was intrerupted
#[ -f fertig.txt ] && rm fertig.txt
# Pre-requirements

#download files eventeuell nur files oder ganzen image
sudo mount -t ubifs ubi0:data /mnt/dataM
sleep 1
#change to other ubi
cd /mnt/dataM
#remove mtd7 update script
echo remove update file form mtd7
sudo flash_eraseall /dev/mtd7
[ -f uImage ] && rm uImage
[ -f efusa7ul.dtb ] && rm efusa7ul.dtb
[ -f rootfs.ubifs ] && rm rootfs.ubifs
[ -f update.scr ] && rm update.scr
#update in mount laden
#command && echo "OK" || echo "NOK"
echo try to download
#kernel
wget -T 20 https://bitbucket.org/MMMMarc/piavitagatewaypro/raw/$UPDATESERVER/releases/$FILENAME/uImage || exit 0
#device tree
wget -T 20 https://bitbucket.org/MMMMarc/piavitagatewaypro/raw/$UPDATESERVER/releases/$FILENAME/efusa7ul.dtb || exit 0
#rootfilesystem
wget -T 20 https://bitbucket.org/MMMMarc/piavitagatewaypro/raw/$UPDATESERVER/releases/$FILENAME/rootfs.ubifs || exit 0
#update script
wget -T 20 https://bitbucket.org/MMMMarc/piavitagatewaypro/raw/$UPDATESERVER/releases/$FILENAME/update.scr || exit 0


cd /mnt/dataM/
sudo nandwrite -p /dev/mtd7 update.scr
sleep 5
cd /home/piavita/application
sudo umount -t ubifs ubi0:data
echo update ready reboot NOW

cd /home/piavita/application
#sudo node LEDs_cyan.js
#cd /home/piavita/application/tmp
#sudo touch fertig.txt
sudo reboot -f
