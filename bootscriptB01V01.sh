#!/bin/sh
cd /home/piavita/application
#color: cyan
cd /home/piavita/application
sudo node LEDs_cyan.js &

if [ ! -f /home/piavita/application/HWsettings.json ]; then
  sudo mount -t ubifs ubi0:data /mnt/dataM
  sleep 1
  cp  /mnt/dataM/HWsettings.json /home/piavita/application/HWsettings.json
  cd
  sudo unmount -t ubifs ubi0:data
  sudo flash_eraseall /dev/mtd7

fi

cd /home/piavita/application
#APN=$(sed -n 's|.*"APN":"\([^"]*\)".*|"\1"|p' /home/piavita/application/ID.json)
UPDATESERVER=$(sed -n 's|.*"updateserver":"\([^"]*\)".*|\1|p' HWsettings.json)
#sudo cat wvdial.sample | sed s/@APN@/"$APN"/g  > wvdial.conf && sudo mv wvdial.conf /etc/

if [ $(cat /sys/class/net/eth0/carrier) == 0 ];then
  echo eth is down
  cd /home/piavita/application
  sudo sh /home/piavita/application/starttoby.sh
  sleep 1
fi


cd /home/piavita/application/
[ ! -d tmp ] && mkdir tmp && chown piavita:piavita tmp
cd /home/piavita/application/tmp

[ -f updatecheckB01.sh ] && sudo rm updatecheckB01.sh

wget https://bitbucket.org/MMMMarc/piavitagatewaypro/raw/"$UPDATESERVER"/updatecheckB01.sh

#chown piavita:piavita updatecheckB01.sh && chmod +x updatecheckB01.sh && sudo -u piavita ./updatecheckB01.sh

#runn application
cd /home/piavita/application/

#sudo node /home/piavita/application/maini.js &
sudo pm2 start /home/piavita/application/maini.js
